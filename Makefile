PYTHON=3.8
CONDA_CH=defaults conda-forge pytorch
BASENAME=$(shell basename $(CURDIR))

# setup
env:
	conda create -n $(BASENAME)  python=$(PYTHON)

setup:
	conda install -y --file requirements.txt $(addprefix -c ,$(CONDA_CH))

# services
triton:
	docker run --gpus all --rm -p 8000:8000 -p 8001:8001 -p 8002:8002 \
		-v $(PWD)/triton/model_repository:/models nvcr.io/nvidia/tritonserver:23.11-py3 \
		tritonserver --model-repository=/models

model_analyzer:
	docker run -it --gpus all -v /var/run/docker.sock:/var/run/docker.sock \
	-v $(PWD)/triton/model_repository:/models \
	-v $(PWD)/triton/model_analyzer/blender-bot-encoder:/model_analyzer \
	--net=host nvcr.io/nvidia/tritonserver:23.11-py3-sdk

profile:
	model-analyzer profile --profile-models blender-bot-encoder --triton-launch-mode=remote \
	--output-model-repository-path /model_analyzer/configs  --override-output-model-repository \
	--latency-budget 10 --run-config-search-mode quick --client-protocol grpc -f /model_analyzer/perf.yaml \
	-s /model_analyzer/checkpoints -e /model_analyzer


perf_analyzer:
	docker run -it --net=host -v $(PWD):/workspace/ nvcr.io/nvidia/tritonserver:23.11-py3-sdk bash

	perf_analyzer -m blender-bot-encoder -b 16 --shape attention_mask:10 --shape input_ids:10 --concurrency-range 2
