import logging
from typing import Optional

logger = logging.gerLogger(__name__)


def clear_config(path: str, path_to_save: Optional[None] = None):
    with open(path, 'r') as f:
        config = f.read()
    clear_values = ['name', 'data_type', 'TYPE_FP32', 'TYPE_INT64',
                    'dims', 'format', 'is_shape_tensor',
                    'allow_ragged_batch', 'FORMAT_NONE', 'optional',
                    'label_filename', 'platform', 'backend', 'max_batch_size',
                    'input', 'output']
    for val in clear_values:
        config = config.replace(f'"{val}"', 'name')
    if not path_to_save:
        path_to_save = path
        logger.warning('The path to save the config was not provided. The output config will be overwritten')
    with open(path_to_save, "w") as output:
        output.write(config)
