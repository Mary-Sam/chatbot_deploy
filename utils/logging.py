import logging


def basic_config(log_filename=None):
    log_formatter = logging.Formatter("%(asctime)s [%(processName)s] [%(levelname)s]  %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    if log_filename is not None:
        file_handler = logging.FileHandler(log_filename, mode="w")
        file_handler.setFormatter(log_formatter)
        handlers = [file_handler, console_handler]
    else:
        handlers = [console_handler]
    logging.basicConfig(level=logging.DEBUG, handlers=handlers)
