# **Развертывание чат-бота в NVIDIA Triton сервере.**

В качестве чат-бота была взята модель с [HuggingFace](https://huggingface.co/facebook/blenderbot_small-90M?text=Hi.).

Для ускорения инференса была произведена конвертация pytorch модели в onnx. Для преобразования необходимо было разделить модель на три:

    - blender-bot-encoder
    - blender-bot-init
    - blender-bot-decoder

Описание трех моделей построено на основе [репозитория](https://github.com/siddharth-sharma7/fast-Bart/tree/main).

Для разворачивания чат-бота использовался Triton сервер. Triton сервер автоматически позволяет скейлить реплики моделей и загружать их на n-GPU. Директория, содержащая модели, имеет определенную структуру. В config.pbtxt описывается конфигурация модели (батч, входы/выходы, настройка динамических батчей, реплик и т.д)

Model Analyzer позволяет подобрать оптимальную конфигурацию для моделей. Результат его работы это графики и репорты, позволяющие проанализировать производительность инференса моделей.

```
Inferences/Second vs. Client Average Batch Latency
Concurrency: 2, throughput: 2513.48 infer/sec, latency 12723 usec
```

### ONNX RT execution on GPU w. CUDA execution provider

Параметры оптимизации:

1. **cudnn_conv_algo_search**: Конфигурация поиска алгоритма свертки CUDA. Доступные параметры равны 0 (дорогостоящий исчерпывающий сравнительный анализ с использованием cudnnFindConvolutionForwardAlgorithmEx). Это также вариант по умолчанию, 1 - HEURISTIC (упрощенный поиск на основе эвристики с использованием cudnnGetConvolutionForwardAlgorithm_v7), 2 - DEFAULT (алгоритм по умолчанию с использованием CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM);
2. **gpu_mem_limit**: Ограничение памяти CUDA.

```
## Additions to Config
parameters { key: "cudnn_conv_algo_search" value: { string_value: "0" } }
parameters { key: "gpu_mem_limit" value: { string_value: "4294967200" } }

## Perf Analyzer Query
perf_analyzer -m blender-bot-encoder -b 16 --shape attention_mask:10 --shape input_ids:10 --concurrency-range 2
...
Inferences/Second vs. Client Average Batch Latency
Concurrency: 2, throughput: 2510.41 infer/sec, latency 12740 usec
```


### ONNX RT execution on GPU w. TRT acceleration (оптимизация TensorRT)

Параметры оптимизации:

1. **precision_mode**: Точность, используемая для оптимизации. Допустимые значения - "FP32", "FP16" и "INT8". Значение по умолчанию - "FP32";
2. **max_workspace_size_bytes**: Максимальная память GPU, которую модель может временно использовать. Значение по умолчанию - 1 ГБ.


```
## Additions to Config
optimization {
  graph : {
    level : 1
  }
 execution_accelerators {
    gpu_execution_accelerator : [ {
      name : "tensorrt",
      parameters { key: "precision_mode" value: "FP16" },
      parameters { key: "max_workspace_size_bytes" value: "1073741824" }
    }]
  }
}

## Perf Analyzer Query
perf_analyzer -m blender-bot-encoder -b 16 --shape attention_mask:10 --shape input_ids:10 --concurrency-range 2
...
Inferences/Second vs. Client Average Batch Latency
Concurrency: 2, throughput: 3392.89 infer/sec, latency 9424 usec
```
