from argparse import ArgumentParser
import os

from transformers.modeling_outputs import (
    Seq2SeqLMOutput,
    BaseModelOutput,
)
import torch
import functools
import operator
from transformers import AutoConfig, \
                         BlenderbotSmallForConditionalGeneration, \
                         BlenderbotSmallTokenizer
import tritonclient.http as httpclient



class BlenderEncoder(torch.nn.Module):
    def __init__(self, client):
        super().__init__()
        self.client = client
        self.main_input_name = "input_ids"

    def forward(
        self,
        input_ids,
        attention_mask,
        inputs_embeds=None,
        head_mask=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
    ):

        input_ids_infer = httpclient.InferInput("input_ids", input_ids.shape, datatype="INT64")
        input_ids_infer.set_data_from_numpy(input_ids.cpu().numpy())
        attention_mask_infer = httpclient.InferInput("attention_mask", attention_mask.shape, datatype="INT64")
        attention_mask_infer.set_data_from_numpy(attention_mask.cpu().numpy())
        results = self.client.infer(model_name="blender-bot-encoder", inputs=[input_ids_infer, attention_mask_infer])
        encoder_hidden_state = results.as_numpy('hidden_states')
        return BaseModelOutput(torch.tensor(encoder_hidden_state))


class BlenderDecoderInit(torch.nn.Module):
    def __init__(self, client):
        super().__init__()
        self.client = client

    def forward(self, input_ids, encoder_attention_mask, encoder_hidden_states):
        input_ids_infer = httpclient.InferInput("input_ids", input_ids.shape, datatype="INT64")
        input_ids_infer.set_data_from_numpy(input_ids.cpu().numpy())
        attention_mask = httpclient.InferInput("encoder_attention_mask", encoder_attention_mask.shape, datatype="INT64")
        attention_mask.set_data_from_numpy(encoder_attention_mask.cpu().numpy())
        hidden_states = httpclient.InferInput("encoder_hidden_states", encoder_hidden_states.shape, datatype="FP32")
        hidden_states.set_data_from_numpy(encoder_hidden_states.cpu().numpy())
        decoder_outputs = self.client.infer(model_name="blender-bot-decoder-intial", inputs=[input_ids_infer, attention_mask, hidden_states])
        output_keys = list(decoder_outputs._output_name_to_buffer_map.keys())
        output_keys_sorted = [(x, int(x.split('_')[-1])) for x in output_keys[2:]]
        output_keys_sorted.sort(key=lambda x: x[1])
        output_keys[2:] = [x[0] for x in output_keys_sorted]
        list_pkv = tuple(torch.from_numpy(decoder_outputs.as_numpy(out)) for out in output_keys[1:])
        x = tuple(torch.from_numpy(decoder_outputs.as_numpy(out)) for out in output_keys)
        out_past_key_values = tuple(
            list_pkv[i : i + 4] for i in range(0, len(list_pkv), 4)
        )
        return torch.from_numpy(decoder_outputs.as_numpy(output_keys[0])), out_past_key_values


class BlenderDecoder(torch.nn.Module):
    def __init__(self, client):
        super().__init__()
        self.client = client

    def forward(self, input_ids, attention_mask, encoder_output, past_key_values):

        decoder_inputs = {
            "input_ids": input_ids.cpu().numpy(),
            "encoder_attention_mask": attention_mask.cpu().numpy(),
        }

        flat_past_key_values = functools.reduce(operator.iconcat, past_key_values, [])

        past_key_values = {
            (f"pkv_{i}.1" if i==11 or i==22 else f"pkv_{i}"): pkv.cpu().numpy() for i, pkv in enumerate(flat_past_key_values)
        }
        decoder_inputs_infer = []
        input_dct = {**decoder_inputs, **past_key_values}
        for k, val in input_dct.items():
            if k == 'encoder_attention_mask' or k == 'input_ids':
                datatype = "INT64"
            else:
                datatype = "FP32"
            item_value = httpclient.InferInput(k, val.shape, datatype=datatype)
            item_value.set_data_from_numpy(val)
            decoder_inputs_infer.append(item_value)

        decoder_outputs = self.client.infer(model_name="blender-bot-decoder", inputs=decoder_inputs_infer)
        output_keys = list(decoder_outputs._output_name_to_buffer_map.keys())
        output_keys_sorted = [(x, int(x.split('_')[-1])) for x in output_keys[2:]]
        output_keys_sorted.sort(key=lambda x: x[1])
        output_keys[2:] = [x[0] for x in output_keys_sorted]

        # converts each value of the list to tensor from numpy
        list_pkv = tuple(torch.from_numpy(decoder_outputs.as_numpy(x)) for x in output_keys[1:])

        # creates a tuple of tuples of shape 6x4 from the above tuple
        out_past_key_values = tuple(
            list_pkv[i : i + 4] for i in range(0, len(list_pkv), 4)
        )

        return torch.from_numpy(decoder_outputs.as_numpy(output_keys[0])), out_past_key_values


class OnnxBlender(BlenderbotSmallForConditionalGeneration):

    def __init__(self, model_or_model_path, client):
        config = AutoConfig.from_pretrained(
            model_or_model_path
        )
        super().__init__(config)

        self.client = client
        self.encoder = BlenderEncoder(self.client)
        self.decoder = BlenderDecoder(self.client)
        self.decoder_init = BlenderDecoderInit(self.client)

    def get_encoder(self):
        return self.encoder

    def get_decoder(self):
        return self.decoder

    def get_output_embeddings(self):
        return None
    
    
    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        decoder_input_ids=None,
        decoder_attention_mask=None,
        head_mask=None,
        decoder_head_mask=None,
        cross_attn_head_mask=None,
        encoder_outputs=None,
        past_key_values=None,
        inputs_embeds=None,
        decoder_inputs_embeds=None,
        labels=None,
        use_cache=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
    ):

        # if encoder_outputs is None:
        #     # Convert encoder inputs in embeddings if needed
        #     encoder_outputs = self.encoder(
        #         input_ids=input_ids, attention_mask=attention_mask
        #     )

        encoder_hidden_states = encoder_outputs[0]

        if past_key_values is not None:
            if decoder_input_ids is not None:
                decoder_input_ids = decoder_input_ids[:, -1:]
            if decoder_inputs_embeds is not None:
                decoder_inputs_embeds = decoder_inputs_embeds[:, -1:]

        if past_key_values is None:

            # runs only for the first time:
            init_onnx_outputs = self.decoder_init(
                decoder_input_ids, attention_mask, encoder_hidden_states
            )

            logits, past_key_values = init_onnx_outputs

        else:

            onnx_outputs = self.decoder(
                decoder_input_ids,
                attention_mask,
                encoder_hidden_states,
                past_key_values,
            )

            logits, past_key_values = onnx_outputs

        return Seq2SeqLMOutput(logits=logits, past_key_values=past_key_values)



if __name__=='__main__':
    parser = ArgumentParser('Convert pytorch-model to onnx')
    parser.add_argument('-p', '--pretrained_version', type=str, default='facebook/blenderbot_small-90M', help='')
    parser.add_argument('-ma', '--max_answer_length', type=int, default=100, help='')
    parser.add_argument('-m', '--model', type=str, default=None, help='')
    parser.add_argument('-op', '--output_path', type=str, default=None, help='')
    parser.add_argument('-i', '--input_sequence_length', type=int, default=None, help='')
    parser.add_argument('-ov', '--onnx_opset_version', type=int, default=None, help='')
    parser.add_argument('-t', '--triton_server_url', type=str, default='localhost:8000', help='')
    
    args = parser.parse_args()

    url = os.environ.get('TRITON_SERVER_URL', args.triton_server_url)
    client = httpclient.InferenceServerClient(url=url)
    model = OnnxBlender(args.pretrained_version, client)
    tokenizer = BlenderbotSmallTokenizer.from_pretrained(args.pretrained_version)
    while True:
        utterance = input('>> User: ')
        inputs = tokenizer(utterance, return_tensors="pt")
        outputs= model.generate(**inputs, max_length=args.max_answer_length)
        response = tokenizer.decode(outputs[0], skip_special_tokens = True)
        print(f"AI: {response}")
        if 'bye' in utterance.lower():
            break
